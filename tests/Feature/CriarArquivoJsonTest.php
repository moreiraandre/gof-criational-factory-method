<?php

namespace Tests\Feature;

use Dados\Pessoa;
use FactoryMethod\Fabrica\GeradorRelatorioJsonFabrica;
use PHPUnit\Framework\TestCase;

class CriarArquivoJsonTest extends TestCase
{
    public function testSucesso()
    {
        $nomeCompletoArquivo = './exemplos/tmp/dados-pessoa.json';

        $dadosPessoa = new Pessoa();
        $dadosPessoa->primeiroNome = 'Fulano';
        $dadosPessoa->ultimoNome = 'da Silva';
        $dadosPessoa->idade = 44;
        $dadosPessoa->telefone = '(11) 99999-9999';

        $geradorRelatorioJsonFabrica = new GeradorRelatorioJsonFabrica();
        $geradorRelatorioJson = $geradorRelatorioJsonFabrica->criarObjeto($nomeCompletoArquivo);
        $geradorRelatorioJson->gerar($dadosPessoa);

        $this->assertFileExists($nomeCompletoArquivo);
        $this->assertJson(file_get_contents($nomeCompletoArquivo));
    }
}