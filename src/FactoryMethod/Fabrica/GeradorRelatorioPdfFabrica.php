<?php

namespace FactoryMethod\Fabrica;

use FactoryMethod\Produto\GeradorRelatorioInterface;
use FactoryMethod\Produto\GeradorRelatorioPdf;
use RelatorioFormatos\Pdf;

class GeradorRelatorioPdfFabrica extends GeradorRelatorioFabricaAbstract
{
    public function criarObjeto(string $nomeCompletoArquivo = null): GeradorRelatorioInterface
    {
        $this->validarTamanhoNomeArquivo($nomeCompletoArquivo);
        return new GeradorRelatorioPdf(new Pdf(), $nomeCompletoArquivo);
    }
}