<?php

namespace FactoryMethod\Fabrica;

use FactoryMethod\Produto\GeradorRelatorioInterface;
use FactoryMethod\Produto\GeradorRelatorioTexto;
use RelatorioFormatos\Xml;

class GeradorRelatorioXmlFabrica extends GeradorRelatorioFabricaAbstract
{
    public function criarObjeto(string $nomeCompletoArquivo = null): GeradorRelatorioInterface
    {
        $this->validarTamanhoNomeArquivo($nomeCompletoArquivo);
        return new GeradorRelatorioTexto(new Xml(), $nomeCompletoArquivo);
    }
}