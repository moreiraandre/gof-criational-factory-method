<?php

namespace FactoryMethod\Fabrica;

use FactoryMethod\Produto\GeradorRelatorioInterface;
use Exception;

abstract class GeradorRelatorioFabricaAbstract
{
    abstract public function criarObjeto(string $nomeCompletoArquivo = null): GeradorRelatorioInterface;

    protected function validarTamanhoNomeArquivo(string $nomeCompletoArquivo = null): void
    {
        if (strlen($nomeCompletoArquivo) > 255) {
            throw new Exception('O nome do arquivo é muito grande.');
        }
    }
}