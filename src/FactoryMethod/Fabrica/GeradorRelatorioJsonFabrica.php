<?php

namespace FactoryMethod\Fabrica;

use FactoryMethod\Produto\GeradorRelatorioInterface;
use FactoryMethod\Produto\GeradorRelatorioTexto;
use RelatorioFormatos\Json;

class GeradorRelatorioJsonFabrica extends GeradorRelatorioFabricaAbstract
{
    public function criarObjeto(string $nomeCompletoArquivo = null): GeradorRelatorioInterface
    {
        $this->validarTamanhoNomeArquivo($nomeCompletoArquivo);
        return new GeradorRelatorioTexto(new Json(), $nomeCompletoArquivo);
    }
}