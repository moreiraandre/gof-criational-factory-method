<?php

namespace FactoryMethod\Produto;

use Dados\DadosInterface;
use RelatorioFormatos\FormatoInterface;

interface GeradorRelatorioInterface
{
    public function __construct(FormatoInterface $formato, string $nomeCompletoArquivo);
    public function gerar(DadosInterface $dados): void;
}