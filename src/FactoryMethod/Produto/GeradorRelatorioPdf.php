<?php

namespace FactoryMethod\Produto;

use Dados\DadosInterface;
use RelatorioFormatos\FormatoInterface;
use Mpdf\Mpdf;

class GeradorRelatorioPdf implements GeradorRelatorioInterface
{
    public function __construct(
        private FormatoInterface $formato,
        private string $nomeCompletoArquivo = './arquivo.pdf'
    ) {}

    public function gerar(DadosInterface $dados): void
    {
        $conteudoArquivo = $this->formato->gerarConteudo($dados);
        $mpdf = new Mpdf(['tempDir' => './']);
        $mpdf->WriteHTML($conteudoArquivo);
        $mpdf->Output($this->nomeCompletoArquivo, 'F');
    }
}