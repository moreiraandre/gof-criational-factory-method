<?php

namespace FactoryMethod\Produto;

use Dados\DadosInterface;
use RelatorioFormatos\FormatoInterface;

class GeradorRelatorioTexto implements GeradorRelatorioInterface
{
    public function __construct(
        private FormatoInterface $formato,
        private string $nomeCompletoArquivo = './arquivo.xml'
    ) {}

    public function gerar(DadosInterface $dados): void
    {
        $conteudoArquivo = $this->formato->gerarConteudo($dados);
        file_put_contents($this->nomeCompletoArquivo, $conteudoArquivo);
    }
}