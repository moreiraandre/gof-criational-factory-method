<?php

namespace Dados;

class Pessoa implements DadosInterface
{
    public string $primeiroNome;
    public string $ultimoNome;
    public int $idade;
    public string $telefone;

    public function toArray(): array
    {
        return [
            'NomeCompleto' => trim("$this->primeiroNome $this->ultimoNome"),
            'Idade' => $this->idade,
            'Telefone' => $this->telefone,
        ];
    }
}