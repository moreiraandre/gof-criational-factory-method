<?php

namespace Dados;

interface DadosInterface
{
    public function toArray(): array;
}