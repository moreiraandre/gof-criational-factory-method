<?php

namespace RelatorioFormatos;

use Dados\DadosInterface;

class Pdf implements FormatoInterface
{
    public function gerarConteudo(DadosInterface $dados): string
    {
        $colunas = '';
        $valores = '';
        foreach ($dados->toArray() as $nomeCampo => $valorCampo) {
            $colunas .= "<th>$nomeCampo</th>";
            $valores .= "<td>$valorCampo</td>";
        }

        return "<table border='1' width='100%'><thead><tr>$colunas</tr></thead><tbody><tr>$valores</tr></tbody></table>";
    }
}