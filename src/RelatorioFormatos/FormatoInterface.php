<?php

namespace RelatorioFormatos;

use Dados\DadosInterface;

interface FormatoInterface
{
    public function gerarConteudo(DadosInterface $dados): string;
}