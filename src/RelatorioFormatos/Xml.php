<?php

namespace RelatorioFormatos;

use Dados\DadosInterface;

class Xml implements FormatoInterface
{
    public function gerarConteudo(DadosInterface $dados): string
    {
        $conteudo = '';
        foreach ($dados->toArray() as $nomeCampo => $valorCampo) {
            $conteudo .= "<$nomeCampo>$valorCampo</$nomeCampo>";
        }

        return "<DadosPessoa>$conteudo</DadosPessoa>";
    }
}