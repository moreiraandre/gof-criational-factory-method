<?php

namespace RelatorioFormatos;

use Dados\DadosInterface;

class Json implements FormatoInterface
{
    public function gerarConteudo(DadosInterface $dados): string
    {
        return json_encode($dados->toArray());
    }
}