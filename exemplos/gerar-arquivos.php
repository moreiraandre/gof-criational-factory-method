<?php

require 'vendor/autoload.php';

$nomeCompletoArquivoSemExtensao = './exemplos/tmp/dados-pessoa';

$geradorRelatorioXmlFabrica = new FactoryMethod\Fabrica\GeradorRelatorioXmlFabrica();
$geradorRelatorioXml = $geradorRelatorioXmlFabrica->criarObjeto("$nomeCompletoArquivoSemExtensao.xml");

$geradorRelatorioJsonFabrica = new FactoryMethod\Fabrica\GeradorRelatorioJsonFabrica();
$geradorRelatorioJson = $geradorRelatorioJsonFabrica->criarObjeto("$nomeCompletoArquivoSemExtensao.json");

$geradorRelatorioPdfFabrica = new FactoryMethod\Fabrica\GeradorRelatorioPdfFabrica();
$geradorRelatorioPdf = $geradorRelatorioPdfFabrica->criarObjeto("$nomeCompletoArquivoSemExtensao.pdf");

$dadosPessoa = new Dados\Pessoa();
$dadosPessoa->primeiroNome = 'Fulano';
$dadosPessoa->ultimoNome = 'da Silva';
$dadosPessoa->idade = 44;
$dadosPessoa->telefone = '(11) 99999-9999';

$geradorRelatorioXml->gerar($dadosPessoa);
$geradorRelatorioJson->gerar($dadosPessoa);
$geradorRelatorioPdf->gerar($dadosPessoa);

echo 'OK' . PHP_EOL;