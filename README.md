# GOF - Criacional - Factory Method
Este projeto tem o intuito de estudar o padrão `Factory Method` do grupo `Criacional` dos padrões de projeto GOF.

[Referência](https://refactoring.guru/pt-br/design-patterns/factory-method)

Neste projeto foi implementado um `Gerador de Relatórios` que pudesse gerar relatórios em qualquer formato.

# Docker
## Criar a imagem
```bash
docker-compose build --no-cache # Apenas uma vez
```

## Entrar no terminal do container
```bash
docker-compose run --rm workspace bash
```

# Instalar dependências composer
```bash
composer install
```

# Exemplos
```bash
php exemplos/gerar-arquivos.php
```
> Serão criados arquivos no `./exemplos/tmp`.

# Testes Automatizados
```bash
bin/phpunit --debug --coverage-text
```